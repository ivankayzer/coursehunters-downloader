<?php

namespace App\Commands;

use App\Services\Storage;
use LaravelZero\Framework\Commands\Command;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Get extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'get {url*}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Get';

    protected $client;
    /**
     * @var Storage
     */
    private $storage;

    /**
     * Get constructor.
     * @param Client $client
     * @param Storage $storage
     */
    public function __construct(Client $client, Storage $storage)
    {
        $this->client = $client;
        $this->storage = $storage;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        foreach ($this->argument('url') as $url) {
            echo "============================" . PHP_EOL;
            $this->download($url);
            echo "============================" . PHP_EOL;
        }
    }

    protected function download($url)
    {
        if (!$url) {
            return;
        }

        $courseName = collect(explode('/', $url))->last();

        echo 'Downloading: ' . $courseName . PHP_EOL;

        $html = new Crawler($this->client->get($url)->getBody()->getContents());

        $bar = $this->output->createProgressBar($html->filter('#lessons-list li link[itemprop="contentUrl"]')->count());

        try {
            $this->storage->createDir($courseName);

            $html->filter('#lessons-list li link[itemprop="contentUrl"]')
                ->each(function ($node) use ($bar, $courseName) {
                    $bar->advance();
                    $fileName = $courseName . '/' . collect(explode('/', $node->attr('href')))->last();

                    $stream = fopen(__DIR__ . '../../../Downloads/' . $fileName, 'w+');

                    $this->client->get($node->attr('href'), ['sink' => $stream]);
                });
            $bar->finish();
        } catch (\Exception $e) {
            $bar->finish();
            echo $e->getMessage();
        }
    }
}
