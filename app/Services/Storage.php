<?php

namespace App\Services;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class Storage
{
    public $local;

    public function __construct()
    {
        $this->local = new FileSystem(new Local(__DIR__ . '/../../Downloads/'));
    }

    /**
     * @param $fileName
     * @param $source
     * @return bool
     * @throws \League\Flysystem\FileExistsException
     */
    public function write($fileName, $source)
    {
        if ($this->local->has($fileName)) {
            return true;
        }

        return $this->local->putStream($fileName, $source);
    }

    public function createDir($dir)
    {
        return $this->local->createDir($dir);
    }
}